import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../model/image_model.dart';
import 'dart:convert';
import 'dart:math';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }

}

class AppState extends State<App> {
  int counter = 0;
  int times =1;
  // Bởi hình lấy từ api ko đọc được, nên lấy hình api của nguồn khác
  var imgEgible = [
  "https://maivanmanh.github.io/503106/images/tdtu1.jpg",
  "https://maivanmanh.github.io/503106/images/tdtu2.jpg",
  "https://maivanmanh.github.io/503106/images/tdtu3.jpg",
  "https://maivanmanh.github.io/503106/images/tdtu4.jpg"
  ];

  String image="https://maivanmanh.github.io/503106/images/tdtu1.jpg";
  fetchImage() {

    print('Hi there! counter=$counter');

    Uri url = Uri.https('jsonplaceholder.typicode.com', 'photos/$counter');
    // 'https://jsonplaceholder.typicode.com/photos/1';

    http.get(url).then(
            (result) {
          var jsonRaw = result.body;
          var jsonObject = json.decode(jsonRaw);


          // var imageModel = ImageModel(jsonObject['id'], jsonObject['url']);
          var imageModel = ImageModel.fromJson(jsonObject);
          //image = imageModel.url!;
          int tmpCounter = counter;
          if(counter>=4){
            Random random = new Random();
            tmpCounter= random.nextInt(4);
          }
          image = imgEgible[tmpCounter];

          print(imageModel.url);
        }
    );

    setState(() {
      counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    var appWidget = new MaterialApp(
      debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(title: Text('Image Viewer - v0.0.10'),),
          body: Container(
            child: Column(
              children: [
                Text('Display list of images here. counter = $counter'),
                Image.network(image),
              ],
            )
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: fetchImage
          ),
        )
    );

    return appWidget;
  }
  
}