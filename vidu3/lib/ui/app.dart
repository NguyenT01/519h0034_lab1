import 'package:flutter/material.dart';
import '../model/image_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class App extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return AppState();
  }

}

class AppState extends State<App> {
  int counter = 0;
  List<ImageModel> images = [];

  fetchImages() async {
    counter++;
    var url = Uri.https('jsonplaceholder.typicode.com', 'photos/$counter');
    var response = await http.get(url);

    print(response.body);

    var jsonObject = json.decode(response.body);
    var imageModel = ImageModel(jsonObject['id'], jsonObject['url']);
    images.add(imageModel);


    print('counter=$counter');
    print('Length of images=${images.length}');

    setState(() {
      // Do nothing
    });
  }

  @override
  Widget build(BuildContext context) {
    var appWidget = MaterialApp(
        home: Scaffold(
          appBar: AppBar(title: Text('My Image Viewer - v0.0.6'),),
          body: ListView.builder(
              itemCount: images.length,
              itemBuilder: (BuildContext context, int index) {
                return Text('Item...$index');
              },
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: fetchImages
          ),
        )
    );

    return appWidget;
  }
}